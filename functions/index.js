const functions = require('firebase-functions')
const admin = require('firebase-admin')
//const cors = require('cors')
admin.initializeApp()

exports.addMessage = functions.https.onRequest((request, response) => {
//return cors(function(request, response) {


	response.set('Access-Control-Allow-Origin', '*');
	response.set('Access-Control-Allow-Methods', 'GET, PUT, POST, OPTIONS');
	response.set('Access-Control-Allow-Headers', '*');

	if (request.method === 'OPTIONS') {
	   response.end();
	}
	else {
		//const text = request.query.title
		const titleText = request.query.title
		const bodyText = request.query.content
		const urlText = request.query.url
		const authorText = request.query.writer

		let timestamp = new Date().getTime();
		

		admin
		.database()
		.ref('/posts')
		.push({
			id: timestamp,
			title: titleText,
			body: bodyText, 
			url: urlText,
			writer: authorText,
			userId: ""
		})
		.then(() => 
			response.json({
				message: 'great !', 
				titleText,
				bodyText,
				urlText,
				authorText
			}))
		.catch(() => {
			response.json({
				message: 'not great '
			})
		})
	}
//})
}) 


/*exports.addMessage = functions.https.onRequest((request, response) => {
	const text = request.query.text
	const secretText = toUpperCase(text)

	admin
	.database()
	.ref('/messages')
	.push({text: secretText})
	.then(() => 
		response.json({
			message: 'great !', 
			text
		}))
	.catch(() => {
		response.json({
			message: 'not great '
		})
	})
}) */