import React from 'react';
//import { Container } from 'react-bootstrap';
import styled from 'styled-components';

const Styles = styled.div`
`;

const Layout = (props) => (
 	<Styles>
 	<div >
		{props.children}
	</div>
	</Styles>
)

/*const Layout = (props) => (
 	<Container>
		{props.children}
	</Container>
)
*/

export default Layout;