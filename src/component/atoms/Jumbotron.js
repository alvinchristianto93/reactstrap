import React from 'react';
import { Jumbotron as Jumbo, Container } from 'react-bootstrap';
import styled from 'styled-components';
import image from '../../assets/background-image.jpg'

const Styles = styled.div`
	.jumbo{
		background: url(${image}) no-repeat fixed bottom;
		background-size: cover;
		color: #3e3e3e;
		height: 300px;
	}
	.overlay{
		background-color: #000;
		opacity: 0.6;
		//position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		z-index: -1;
	}
	.textcenter{
		padding-top: 100px;
	 	text-align: center;
	 	font-family: Arial;
	}
`;
/*style={{textAlign: "center"}}*/

const Jumbotron = () => (
	<Styles>
		<Jumbo fluid className="jumbo">
		<div className="overlay"></div>
			<Container className="textcenter">
				<h3>Quotes of the Day</h3>
				<i>source : 
					<a href="google.com"> theysaidso.com/api</a>
				</i>
			</Container>
		</Jumbo>
	</Styles>
)

export default Jumbotron;