import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import styled from 'styled-components';
import imgLogo from '../../assets/logo.png';

const Styles = styled.div`
	.navbar{
		background-color: #dee7f1;
	}
	.navbar-brand, .navbar-nav .nav-link{
		color: #ece8e8;

		&:hover {
			color: white;	
		}
	}
`;

const NavigationBar = () =>(
	<Styles>
		 <Navbar expand="lg" fixed="top">
		 	<Navbar.Brand href="/">
		 	 <img
		      src= { imgLogo }
			  width="40"
		      height="40"
		      className="d-inline-block align-top"
		      alt="logo"
		     />
		     {/*<nav class="navbar navbar-default navbar-fixed-top">
    */}
		 	</Navbar.Brand>
		 {/*		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		 	<Navbar.Collapse id="basic-navbar-nav">
		 		<Nav className="ml-auto">
		 		<Nav.Item><Nav.Link href="/">Home</Nav.Link></Nav.Item>
		 			<Nav.Item><Nav.Link href="/about">about</Nav.Link></Nav.Item>
		 			<Nav.Item><Nav.Link href="/contact">contact</Nav.Link></Nav.Item>
		 			<Nav.Item><Nav.Link href="/login">login</Nav.Link></Nav.Item>
		 			<Nav.Item><Nav.Link href="/signup">signup</Nav.Link></Nav.Item>
		 		</Nav>
		 	</Navbar.Collapse>*/}
		 </Navbar>
	</Styles>

)

export default NavigationBar;