import firebase from '../../config/firebase';


export const firequery = {
    postDatatoFirebase,
    loginWithFirebase
};

function loginWithFirebase(values){
    return new Promise((resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(values.email, values.password)
        .then(function(res){
            console.log(res)
            return resolve(res)
        }).catch(function(error){
            console.log(error)
            return reject(error)
        });
    });

}


function postDatatoFirebase(values){
    return new Promise((resolve, reject) => {
        var errorCode
        var errorMessage
        console.log("postDatatoFirebase then do callback "+values);
        firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
        .then(function(res){
            console.log("successfully created firebase "+values.email)
            alert("An Email has sent to  "+values.email+ ", please activate your account");
            resolve();
        })
        .catch(function(error) {
            errorMessage = error.message;
            errorCode = error.code;
            console.log(errorMessage + ' ( ' + values.email + ' )');
            alert(errorMessage + " " + values.email + ", code " + errorCode);
            reject();
        });
   
    });
}
/*function postDatatoFirebase(values, callback){
    var errorCode
    var errorMessage
    console.log("postDatatoFirebase then do callback "+values);
    firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
    .then(function(user){
        console.log("successfully created firebase "+values.email)
        alert("An Email has sent to  "+values.email+ ", please activate your account");
        callback(values)
        
      })
    .catch(function(error) {
        errorMessage = error.message;
        errorCode = error.code;
        console.log(errorMessage + ' ( ' + values.email + ' )');
        alert(errorMessage + " " + values.email + ", code " + errorCode);
      //return reject();
        
    });*/
  
