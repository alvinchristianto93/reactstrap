import React from 'react';
import { Form, Alert} from 'react-bootstrap';

import styled from 'styled-components';
//import imgLogo from '../assets/logo.png';

const Styles = styled.div`
  .info-signup{
    padding-right: 10px;
    padding-top: 10px;
  }
  .info-signup p {
    font-size: 15px;
    padding-top: 10px;
  }
  .container-wrap{
    margin-top: 20px;
  }
  .logo{
    display: block;
    margin-left: auto;
    margin-right: auto;
    width : 50%;
  }
  .redirect-signup{
    margin-top: 20px;
  }
  .redirect-signup p{
    text-align: center;
    font-size: 12px;
    color: #515252;
  }
  
  @media (max-width: 768px) {
   #info-extra{
    display: none;
   }
  }
`;



const Sideinfo = () =>{
  return(
    <Styles>
        <div>
          <h4> Join the Stack Overflow community</h4>
          <hr />
          <p> - Unlock new privileges like voting and commenting</p>
          <p> - Save your favorite tags, filters, and jobs</p>
          <p> - Earn reputation and badges</p>
          <Form.Text className="text-muted">
           Use the power of Stack Overflow inside your organization.
           Try a 
           <Alert.Link href="#" Style="font-weight:normal; text-decoration:none; "> free trial of Stack Overflow for Teams </Alert.Link> .
          </Form.Text>
         </div>
    </Styles>
     )
}

export default Sideinfo;