import React /*{ Fragment }*/ from 'react';
import { connect } from "react-redux";
import { addItem } from "./_actions";
import { Container } from 'react-bootstrap';
import ShowItem from './aboutshow';

class About extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			aboutItem : ""
		}
	}
	//handleOnChange(input){ 
	handleOnChange = (input) =>{
		this.setState({
			aboutItem: input
		})
	}
	//handleSubmit(){
	handleSubmit = () =>{
		console.log("handleSubmit "+this.state.aboutItem)
		this.props.addItem(this.state.aboutItem);
    this.setState(
      { 
        aboutItem: "" 
      }
    );
	}
	render(){
		return(
		 	<Container>
				<div>
					<p>
						this is about
					</p>
		      <input
		        onChange={e => this.handleOnChange(e.target.value)}
		        value={this.state.aboutItem}
		      />
		      <button onClick={this.handleSubmit}>
		        Add
		      </button>
		      <ShowItem />
				</div>

			</Container>
			);
		}
}


export default connect(null ,{ addItem })(About);


