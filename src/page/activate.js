import React from 'react';

const activate = () =>{
	return(
		<div>
			<h2> A Confirmation Email has been sent to you </h2>
			<p>
				Please check your inbox for an email 	
			</p>
		</div>
	);
}

export default activate;