import React, {Component, Fragment} from 'react';
//import Post from './Post.js';
import firebase from '../config/firebase';

import styled from 'styled-components';
import axios from 'axios';


const Styles = styled.div`
	.quotes_body{
		font-size: 18px;
	 	text-align: center;
	 	font-family: Auto;
	 	font-style: italic;
	 },
	 .quotes_writer{
		font-size: 20px;
	 	text-align: right;
	 	font-style: italic;
	 },
	 .quote_desc{
    width: 90%;
    height: 150px;
	 },
	 .quote-box{

	 }
`;

class Showcontent extends Component {
	constructor(props) {
	    super(props)
	    this.state = {
	    	post: [],
	    	formBlogPost:{
	    		id: 1,
	    		title: "",
	    		body: "",
	    		url: "",
	    		writer: "",
	    		userId: ""
	    	},
	    	title: [],
	    	body: [],
	    	url: [],
	    	writer: [],
	    	table: '',
	    	newPost: [],
	    	isUpdate: false
	    	}
	   }
	//read json file
	getPostAPI = () =>{
		axios.get(`http://localhost:3004/posts?_sort=id&_order=desc`)
  		.then((result) => {
  			this.setState({
  				post: result.data
  			})
    			// console.log(result.data);
  		})
	}

	postDataToAPI = () => {
//		axios.post('http://localhost:3004/posts', this.state.formBlogPost)
		const title = this.state.formBlogPost.title
		const content = this.state.formBlogPost.body
		const url = this.state.formBlogPost.url
		const writer = this.state.formBlogPost.writer
		//console.log(title+content+url+writer)
		axios.post(`http://us-central1-restrap-firebase.cloudfunctions.net/addMessage?title=${title}&content=${content}&url=${url}&writer=${writer} `)
		.then(res => {
			this.getPostAPI();
			this.setState({
				isUpdate: false,
				formBlogPost:{
		    		id: 1,
		    		title: "",
		    		body: "",
	    			url: "",
	    			writer:"",
		    		userId: ""
	    		}
			});
		}, (err) => {
			console.log(err)
		} )
	}

	//update data
	putDataToAPI = () => {
		axios.put(`http://localhost:3004/posts/${this.state.formBlogPost.id}`,this.state.formBlogPost )
		.then((res) =>{
			console.log(res); 
			this.getPostAPI();
			this.setState({
				isUpdate: false,
				formBlogPost:{
		    		id: 1,
		    		title: "",
		    		body: "",
		    		userId: ""
	    		}
			});

		});
	
	}

	//delete then call getPostAPI
	handleDelete = (data) => {
		console.log(data)
		//why back tick ``??
		axios.delete(`http://localhost:3004/posts/${data}`)
		.then((res) =>{
			this.getPostAPI();
			//console.log(res);
		})
	}

	handleUpdate = (data) => {
		this.setState({
			formBlogPost: data,
			isUpdate: true
		})
		console.log(this.state.formBlogPost);
	}

	handleShowPerContent = (data) => {

    //data.preventDefault();
		console.log("doing click ",data)
		//why back tick ``??
	}

	handleFormChange = (event) => {
		let timestamp = new Date().getTime();
		let formBlogPostNew = {...this.state.formBlogPost};

		if(!this.state.isUpdate){
			formBlogPostNew['id'] = timestamp
		}
		formBlogPostNew[event.target.name] = event.target.value
		this.setState({
			formBlogPost: formBlogPostNew 
		}, () => {
			console.log("value formBlogPost", this.state.formBlogPost);
		})
	}

	handleSubmit = () =>{
		if(this.state.isUpdate){
			this.putDataToAPI();
		}else{
			this.postDataToAPI();
		}
	}
	componentDidMount(){
			//this.getPostAPI();
   	
  		const rootRef = firebase.database().ref();
  		//const posts = rootRef.child('posts');
  		const posts = rootRef.child('posts').orderByChild('id').limitToLast(4)	;
  		posts.once('value', snap => {
    	snap.forEach( row => {
    			//console.log(row.val().writer)
          this.setState({
            title: this.state.title.concat([row.val().title]),
            body: this.state.body.concat([row.val().body]),
            writer: this.state.writer.concat([row.val().writer])
            //url: this.state.url.concat([row.val().url])
          });
         });
    	 
    	if(this.state.title.length > 0) {
    		var index_length = this.state.title.length - 1 //index start at 0
	    	const tableList = this.state.body.map((name, index) =>
	     	<Fragment>
		          <h3 className="quotes_body">"{this.state.body[index_length - index]}"</h3>
		          <p className="quotes_writer">~ {this.state.writer[index_length - index]}</p>
		          <hr/>
	     	</Fragment>
	      );
	    	
	    	this.setState({
	    	  table:tableList
	    	})
	    };
	   

 		/*const ids = '-M7rueY9-eaxNcC67aFH'
 		return firebase.database().ref('/posts/' + ids).once('value')
 		.then(function(snapshot) {
 			var username = (snapshot.val() && snapshot.val().url) || 'Anonymous';
 			console.log(username);
  		})*/
  	})


  	}

	render() {
	     return (
				<Styles>
	     	<Fragment>
	     		<div className="quote-box">
{/*				
			<div className="form-post">
					<p className="title">Title </p>
					<input type="text" name="title" value={this.state.formBlogPost.title} onChange={this.handleFormChange}/>
					
					<p className="url">Url </p>
					<input type="text" name="url" value={this.state.formBlogPost.url} onChange={this.handleFormChange}/>
					
					<p className="post-desc" >Quote Desc</p>
					<textarea className="quote_desc" name="body" value={this.state.formBlogPost.body} onChange={this.handleFormChange}></textarea>
					
					<p className="Author">Author </p>
					<input type="text" name="writer" value={this.state.formBlogPost.writer} onChange={this.handleFormChange}/>
					<br/>
					<button className="submit-btn" onClick={this.handleSubmit}>submit</button>
				</div>*/}
					
  			{/*	{	
						this.state.post.map(post => {
							return  <Post 
								key={post.id} 
								data={post} 
								//remove={this.handleDelete} 
								//update={this.handleUpdate}
								goDetail={this.handleShowPerContent}
								/>
							//return  <Post key={post.id} data={post} remove={this.handleDelete} update={this.handleUpdate}/>
						})
					}
*/}
					{this.state.table.length > 0 ? 
						this.state.table 
						:null}
				</div>
			</Fragment>
			</Styles>
	   )}
}

export default Showcontent;