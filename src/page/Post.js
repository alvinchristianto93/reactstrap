import React, {Component, Fragment} from 'react';

import styled from 'styled-components';


const Styles = styled.div`
  .title{
    font-size: 30px;
    text-decoration: none;
  }`;

const Post = (props) => {
	console.log(props);

	return(
		<Styles>
		<div className="post">
			{/*<div className="img-thumb">
				<img className="img-icon" src="https://placeimg.com/150/100/arch" />
			</div>*/}
			<div className="main-content">
				{/*<div className="content">
					<h3 className="title" onClick={() => props.goDetail(props.data.id)}>{props.data.title}</h3>
					<p className="desc">{props.data.body}</p>
					<button className="update-btn" onClick={() => props.update(props.data)} >Update</button>
					<button className="remove-btn" onClick={() => props.remove(props.data.id)} >Remove</button>
				</div>*/}

					<div className="content">
					<a 
						className="title" 
						onClick={() => props.goDetail(props.data.url)}
						href={'http://localhost:3004'+props.data.url}> 
							{props.data.title}
						</a>
					<p 
						className="desc">{props.data.body}</p>
				</div>		
			</div>
			<hr/>
		</div>
		</Styles>	
	)
} 
export default Post;

