import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import additem from './additem.reducer';

const rootReducer = combineReducers({
  additem,
  authentication,
  registration,
  users,
  alert
});

export default rootReducer;