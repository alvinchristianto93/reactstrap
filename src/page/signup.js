import React, { /*useEffectuseState,*/ } from 'react';
import { useDispatch/*, useSelector*/ } from 'react-redux';
import { useFormik } from 'formik';
//import axios from 'axios';
import { Button, Form, Container, Row, Col, Alert} from 'react-bootstrap';
import Sideinfo from './Sideinfo';

//import firebase from "../config/firebase";

import { userActions } from './_actions';


import styled from 'styled-components';
//import imgLogo from '../assets/logo.png';

const Styles = styled.div`
  .info-signup{
    padding-right: 10px;
    padding-top: 10px;
  }
  .info-signup p {
    font-size: 15px;
    padding-top: 10px;
  }
  .logo{
    display: block;
    margin-left: auto;
    margin-right: auto;
    width : 50%;
  }
  .redirect-signup{
    margin-top: 20px;
  }
  .redirect-signup p{
    text-align: center;
    font-size: 12px;
    color: #515252;
  }
  
  @media (max-width: 768px) {
   #info-extra{
    display: none;
   }
  }
`;


const validate = values => {
  const errors = {};
  if (!values.nickname){
    errors.nickname = 'Required';
  } else if (values.nickname.length > 10){
    errors.nickname = 'no more than 10 character';
  } else if (values.nickname.length < 6){
    errors.nickname = 'at least 6 character';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  } else if (values.password.length < 6 ) {
    errors.password = 'password at least 6 character';
  } else if (!/\d/.test(values.password)) {
    errors.password = 'password need minimum 1 number';
  } else if (/[^0-9a-zA-Z]/.test(values.password)) {
    errors.password = 'password need only letter or number';
  }

  return errors;
};

const SignupForm = () =>{
  const formik = useFormik({
    initialValues: {
      nickname: '',
      email: '',
      validate: '0',
      password: '',
    
   /* const postDatatoAPI = (values) => {
      axios.post('http://localhost:3004/new_user', values)
      .then((res) =>{
        console.log(res);
      }, (err) => {
        console.log(err);
      } );
    }
*/
    }, 
    validate,
    onSubmit: values => {
      //console.log(values);
//    postDatatoAPI(values);
//    postDatatoFirebase(values) ;   
      dispatch(userActions.register(values));   //1. dispatch with action/user.actions.js -> register
    

    }
  });
  
  /*const [user, setUser] = useState({
        firstName: '',
        lastName: '',
        username: '',
        password: ''
    });*/

    //const [submitted, setSubmitted] = useState(false);
    //const registering = useSelector(state => state.registration.registering);
    const dispatch = useDispatch();

    
///
/*
  const postDatatoFirebase = (values) => {
    var errorCode
    var errorMessage

    firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
    .then(function(user){
        console.log(user)
        alert('user successfully created ( ' + values.email + ' )');
     })
    .catch(function(error) {
        errorMessage = error.message;
        errorCode = error.code;
        alert(errorMessage + ' ( ' + values.email + ' )');
    });


  }
*/  
/*const postDatatoAPI = (values) => {
      axios.post('http://localhost:3004/new_user', values)
      .then((res) =>{
        console.log(res);
      }, (err) => {
        console.log(err);
      } );
    }
*/  
  return(
    <Styles>
     <Container className="container-wrap">
       <Row className="justify-content-md-center ">
         <Col xs="12" md="4" >
         <div className="form-wrap">
         <Form onSubmit={formik.handleSubmit}>
              <Form.Group controlId="formNickName">
                <Form.Label>Display Name</Form.Label>
                <Form.Control placeholder="Your name profile" 
                  name="nickname"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.nickname}
                />
                {formik.touched.nickname && formik.errors.nickname ? (
                    <div Style="color: #d20b0b  "> {formik.errors.nickname}</div>
                ) : (
                    <Form.Text className="text-muted" >
                      This name will display as your nick name
                    </Form.Text>
                )}
                
               </Form.Group>

             <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email"  
                  name="email"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                />
                {formik.touched.email && formik.errors.email ? (
                    <div Style="color: #d20b0b  "> {formik.errors.email}</div>
                ) : null }
                
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" 
                  name="password"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                />
                {formik.touched.password && formik.errors.password ? (
                    <div Style="color: #d20b0b  "> {formik.errors.password}</div>
                ) : (
                  <Form.Text className="text-muted">
                    Passwords must contain at least six characters, including at least 1 letter and 1 number.
                  </Form.Text>
                )}
               
              </Form.Group>
              
              <Button variant="primary" type="submit" >
                Sign up
              </Button>
              <Form.Text className="text-muted">
                By clicking “Sign up”, you agree to our 
                <Alert.Link href="#" Style="font-weight:normal; text-decoration:none;"> terms of service</Alert.Link> .
              </Form.Text>
            </Form>        
          </div>
         </Col>

         <Col xs="6" md="4" id= "info-extra" className = "info-signup" >
          <Sideinfo />
         </Col>
       </Row>
     </Container>
    </Styles>
     )
}

export default SignupForm;