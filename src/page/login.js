import React/*, { Component } */from 'react';
import { useDispatch/*, useSelector*/ } from 'react-redux';
import { useHistory } from "react-router-dom"
import axios from 'axios';
import { useFormik } from 'formik';
import { connect } from 'react-redux';
import { actionUserName } from '../config/redux/actionDispatch';

import { Button, Form, Container, Row, Col, Image, Alert} from 'react-bootstrap';
import styled from 'styled-components';
import imgLogo from '../assets/logo.png';


import { userActions } from './_actions';

const Styles = styled.div`
  .info-login{
    padding-right: 10px;
    padding-top: 10px;
  }
  .logo{
    display: block;
    margin-left: auto;
    margin-right: auto;
    width : 20%;
  }
  .redirect-signup{
    margin-top: 20px;
  }
  .redirect-signup p{
    text-align: center;
    font-size: 12px;
    color: #515252;
  }
`;


/*class login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      valueLogin: {
        email: '',
        password: '',
      }
    };
  }
  handleClickSubmit = (event) => {
    event.preventDefault();
    this.getPostAPI();
  }
  handleFormChange = (event) => {
    let valueLoginNew = {...this.state.valueLogin};
    valueLoginNew[event.target.name] = event.target.value
    this.setState({
      valueLogin: valueLoginNew
    })
    console.log(this.state.valueLogin);

  }

  getPostAPI = () =>{
    const email = this.state.valueLogin.email;
    const password = this.state.valueLogin.password;   
    axios.get(`http://localhost:3004/new_user?email=${email}&password=${password}`)
      .then((result) => {
          console.log(result);
        })
      }


  changeUserName = () => {
   this.props.changeUserName()  
  }*/
/////////////////////////


const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.password) {
    errors.password = 'Required';
  } 

  return errors;
};

const LoginForm = () =>{
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    }, 
    validate,
    onSubmit: values => {
      //getDatafromAPI(values);
      console.log('do submit button')
      dispatch(userActions.login(values));   //1. dispatch with action/user.actions.js -> register
    
   }

  
  });
  
  const dispatch = useDispatch();
  
  let history = useHistory();
  /*const getDatafromAPI = (values) => {
      const email = values.email;
      const password = values.password;

      axios.get(`http://localhost:3004/new_user?email=${email}&password=${password}`)
      .then((result) => {
          const res = result.data[0];
          console.log(res);
          if (typeof res !== 'undefined'){
            history.push('/about');

          }
          else {
            console.log("false go on");
          }
        }, (err) => {
        console.log(err);
        } )
    }*/


  return(
    <Styles>
     <Container className="container-wrap">
       <Row className="justify-content-md-center">
         <Col xs="12" md="6" >
         <Image src={ imgLogo } className="logo" roundedCircle /> 
         
       {/*  <p>login for {this.props.userName}</p>
         <button onClick={ this.changeUserName } >click user</button>
         */}
         <div className="form-wrap">
         <Form onSubmit={formik.handleSubmit}>
             <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" 
                  placeholder="Enter email" 
                  name="email" 
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.email}
                />
                {formik.touched.email && formik.errors.email ? (
                    <div Style="color: #d20b0b  "> {formik.errors.email}</div>
                ) : (
                    <Form.Text className="text-muted" >
                      We'll never share your email with anyone else.
                    </Form.Text>
                )}
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" 
                  placeholder="Password" 
                  name="password" 
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.password}
                />
                {formik.touched.password && formik.errors.password ? (
                    <div Style="color: #d20b0b  "> {formik.errors.password}</div>
                ) : null }
                
              </Form.Group>
              <Form.Group controlId="formBasicCheckbox">
                <Form.Check type="checkbox" label="Check me out" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>        
          </div>
          <div className="redirect-signup">
           <p>Don`t have an account?  
            <Alert.Link href="/signup" Style="font-weight:normal; text-decoration:none;"> Sign Up</Alert.Link>
            </p> 
          </div>
         </Col>
       </Row>
     </Container>
    </Styles>
     )
    }




const reduxState = (state) => ({
  popupProps: state.popup, 
  userName: state.userName
})

const reduxDispatch = (dispatch) => ({
  changeUserName : () => dispatch(actionUserName())
})

export default connect(reduxState, reduxDispatch)(LoginForm);