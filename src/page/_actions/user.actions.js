//import { userConstants } from '../_constants';
import { userService } from '../_services';
//import { alertActions } from './';
import { history } from '../_helpers';

import { firequery } from '../_helpers';

export const userActions = {
    login,
    //logout,
    register
    //getAll,
    //delete: _delete
};

function login(user){
    console.log("do login 2")
    firequery.loginWithFirebase(user)
    .then(function(result) {
        console.log("do login check firebase " +result)
    })
    .catch(function(error){
        console.log("do error "+error)
        alert(error);
    });
    
}


function register(user){
    //firequery.postDatatoFirebase(user, registertoLocal)
    firequery.postDatatoFirebase(user)
    .then(result => {
        return registertoLocal(user);
    });
    
}

function registertoLocal(user){
    return new Promise((resolve, reject) => {
    console.log("start registertoLocal "+user)
    userService.register(user)  // make request login with services.userSrvice
    .then(function(user){ 
        console.log("done register to localstorage")
        history.push('/activate_email');
        resolve()
        //1. dispatch  to alertActions success with message
    })
    .catch(function(error) {
        console.log('erroreeerr')
        reject()
    });
        
   });

}
/*
function registertoLocal_bk(user){
    console.log("1 docalback do in registertoLocal")    
    return dispatch => {
        dispatch(request(user)); // make request to constant.userConstants

        //firequery.postDatatoFirebase(user)
        console.log(" 2 docalback do in registertoLocal")    
        userService.register(user)  // make request login with services.userSrvice
            .then(
                user => { 
        
                    console.log("done register to localstorage")
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));  
                    //1. dispatch  to alertActions success with message
                },
                error => {
                    console.log('erroreeerr')
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    
    
    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}
*/

