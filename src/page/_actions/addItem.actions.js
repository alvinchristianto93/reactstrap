import { ADD_ITEM } from "../_constants";

let nextTodoId = 0;
let additional = "add-"

export const addItem = content => ({
  	type: ADD_ITEM,
  	payload: {
    	id: ++nextTodoId,
    	content,
    	additional
  	}
});


