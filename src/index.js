import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import { App } from './containers/App';
import * as serviceWorker from './serviceWorker';
//import firebase from './config/firebase';


// setup fake backend

import { store } from './page/_helpers';
import { configureFakeBackend } from './page/_helpers';
configureFakeBackend();


ReactDOM.render(
	<Provider store={store}>
        <App />
    </Provider>, document.getElementById('root')
	);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
