import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';


var firebaseConfig = {
    apiKey: "AIzaSyDd_HwICAGk-nr5J5EYxu6KYTJWZeYUuXE",
    authDomain: "restrap-firebase.firebaseapp.com",
    databaseURL: "https://restrap-firebase.firebaseio.com",
    projectId: "restrap-firebase",
    storageBucket: "restrap-firebase.appspot.com",
    messagingSenderId: "232568532789",
    appId: "1:232568532789:web:0f055c1246b276dfede67c",
    measurementId: "G-6CBW54RZ64"
  };
  // Initialize Firebase
  // firebase.initializeApp(firebaseConfig);
  firebase.initializeApp(firebaseConfig);

  //firebase.analytics();

  // Get a reference to the database service

export default firebase;

//export default fire;