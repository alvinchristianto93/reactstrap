
const initState = {
  popup: false,
  isLogin: false, 
  userName: 'alvin'
}

const reducer = (state=initState, action) => {
  if(action.type === 'CHANGE_POPUP'){
    return{
      ...state, 
      popup: action.value
    }
  }
  if(action.type === 'CHANGE_ISLOGIN'){
    return{
      ...state, 
      popup: action.value
    }
  }
  if(action.type === 'CHANGE_USER'){
    return{
      ...state, 
      userName: action.value
    }
  }

  return state;
}

export default reducer;