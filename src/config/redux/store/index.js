import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware  from 'redux-thunk';
import { createLogger } from 'redux-logger';
//import reducer from '../reducer';
import rootReducer from '../../../page/_reducers';

export const store = createStore(
	rootReducer,
	applyMiddleware(
		thunkMiddleware
	)
)
