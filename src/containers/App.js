import React, { Fragment, useEffect} from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { history } from '../page/_helpers';
import { alertActions } from '../page/_actions';
//import { store } from '../page/_helpers';
import { PrivateRoute } from '../page/_components';

import home from '../page/home';
import about from '../page/about';
import contact from '../page/contact';
import nomatch from '../page/nomatch';
import login from '../page/login';
import signup from '../page/signup';
import activate from '../page/activate';
import { Loginin } from '../page/Loginin';

import SignupForm from './SignupForm';

import Layout from '../component/atoms/Layout';
import NavigationBar from '../component/atoms/NavigationBar';
import Jumbotron from '../component/atoms/Jumbotron';
import styled from 'styled-components';




const Styles = styled.div`
  .jumbotron{
    height: 100%;
    background-color: #ffffff;
  }
`;

function App() {
  const alert = useSelector(state => state.alert);
  const dispatch = useDispatch();

  useEffect(() => {
      history.listen((location, action) => {
          // clear alert on location change
          dispatch(alertActions.clear());
      });
  }, []);

  return(
        <Styles>
   <Fragment>
      <NavigationBar />
       <Layout>
       <Jumbotron/>   {/*text with background*/}

       {/*<Showcontent/>
*/}
       <div className="jumbotron">
            <div className="container">
                <div className="col-md-8 offset-md-2">
                    {alert.message &&
                        <div className={`alert ${alert.type}`}>{alert.message}</div>
                    }
                </div>
                    <Router  history={history}>
                      <Switch>
                       <PrivateRoute exact path="/" component={home} />
                       <Route path="/loginin" component={Loginin} />
                       <Route path="/about" component={about} />
                       <Route path="/contact" component={contact} />
                       <Route path="/login" component={login} />
                       <Route path="/signup" component={signup} />
                       <Route path="/SignupForm" component={SignupForm} />
                       <Route path="/activate_email" component={activate} />
                       <Route component={nomatch} />
                       <Redirect from="*" to="/" />
                      </Switch>
                    </Router>
            </div>
        </div> 
      </Layout>      
    </Fragment>
      </Styles>
  );
  
}


export { App };
